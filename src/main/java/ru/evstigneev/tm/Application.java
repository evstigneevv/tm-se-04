package ru.evstigneev.tm;

import ru.evstigneev.tm.command.Bootstrap;

public class Application {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}