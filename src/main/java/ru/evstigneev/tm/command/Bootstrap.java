package ru.evstigneev.tm.command;

import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.repository.ProjectRepository;
import ru.evstigneev.tm.repository.TaskRepository;
import ru.evstigneev.tm.service.ProjectService;
import ru.evstigneev.tm.service.TaskService;

import javax.xml.bind.ValidationException;
import java.util.Collection;
import java.util.Scanner;

public class Bootstrap {

    private final Scanner scanner = new Scanner(System.in);
    private final TaskRepository taskRepository = new TaskRepository();
    private final ProjectService projectService = new ProjectService(new ProjectRepository(), taskRepository);
    private final TaskService taskService = new TaskService(taskRepository);
    private String input;

    public String getInput() {
        return input;
    }

    public void setInput() {
        this.input = scanner.nextLine();
    }

    public void init() {
        System.out.println("Welcome to Task Manager!");
        System.out.print("input command or \"exit\" to exit...");
        do {
            setInput();
            switch (getInput().toUpperCase()) {
                case TerminalCommand.HELP:
                    showHelp();
                    break;
                case TerminalCommand.CREATE_PROJECT:
                    createProject();
                    break;
                case TerminalCommand.CREATE_TASK:
                    createTask();
                    break;
                case TerminalCommand.SHOW_TASK:
                    showTaskList();
                    break;
                case TerminalCommand.SHOW_PROJECT_LIST:
                    showProjectList();
                    break;
                case TerminalCommand.DELETE_PROJECT:
                    deleteProject();
                    break;
                case TerminalCommand.UPDATE_PROJECT:
                    updateProject();
                    break;
                case TerminalCommand.UPDATE_TASK:
                    updateTask();
                    break;
                case TerminalCommand.DELETE_TASK:
                    deleteTask();
                    break;
                case TerminalCommand.SHOW_ALL_TASKS:
                    showAllTasks();
                    break;
                case TerminalCommand.SHOW_ONE_TASK:
                    showTask();
            }
        }
        while (!exit());
    }

    public void showHelp() {
        System.out.println("List of commands: ");
        System.out.println("help - show available commands");
        System.out.println("cp - create new project");
        System.out.println("ct - create new task for current project");
        System.out.println("st - show current project's tasks");
        System.out.println("spl - show project list");
        System.out.println("dp - delete project by name");
        System.out.println("up - update current project");
        System.out.println("ut - update current project's task name");
        System.out.println("dt - delete current project's task");
        System.out.println("sat - show all tasks");
        System.out.println("exit - close application");
    }

    public void createProject() {
        System.out.print("input new project name: ");
        input = scanner.nextLine();
        try {
            projectService.persist(input);
            System.out.println("project \"" + input + " \" created ");
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public void createTask() {
        showProjectList();
        System.out.println("input project ID: ");
        input = scanner.nextLine();
        System.out.println("input new task name into project: ");
        try {
            taskService.persist(scanner.nextLine(), input);
            System.out.println("task \"" + input + " \" created ");
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public void showTask() {
        showAllTasks();
        System.out.println("input task ID: ");
        input = scanner.nextLine();
        try {
            Task task = taskService.findOne(input);
            System.out.println(task.getId() + " - task ID | " + task.getName() + " - task name");
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public void showTaskList() {
        showProjectList();
        System.out.println("input project ID: ");
        input = scanner.nextLine();
        try {
            Collection<Task> taskList = taskService.getTaskListByProjectId(input);
            for (Task t : taskList) {
                System.out.println(t.getId() + " - task ID | " + t.getName() + " - task name");
            }
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public void deleteProject() {
        showProjectList();
        System.out.println("input project ID: ");
        input = scanner.nextLine();
        try {
            if (projectService.remove(input)) {
                System.out.println("project deleted!");
            } else {
                System.out.println("Project wasn't deleted");
            }
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public void updateProject() {
        System.out.println("input project ID");
        input = scanner.nextLine();
        System.out.println("input project new name");
        try {
            projectService.update(input, scanner.nextLine());
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public void updateTask() {
        System.out.println("enter task ID");
        input = scanner.nextLine();
        try {
            taskService.update(input, scanner.nextLine());
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public void deleteTask() {
        input = scanner.nextLine();
        try {
            taskService.remove(input);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public boolean exit() {
        return input.equals("exit");
    }

    public void showProjectList() {
        try {
            Collection<Project> projectList = projectService.findAll().values();
            if (projectList.isEmpty()) {
                System.out.println("There are no projects!");
            } else {
                for (Project project : projectList) {
                    System.out.println("Project uuid: " + project.getId() + " | Project name: " + project.getName());
                }
            }
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public void showAllTasks() {
        if (taskService.findAll().isEmpty()) {
            System.out.println("There are no tasks!");
        } else {
            for (Task t : taskService.findAll().values()) {
                System.out.println(t.getProjectId() + " - project ID | " + t.getId() + " - task ID | "
                        + t.getName() + " - task name");
            }
        }
    }

}
