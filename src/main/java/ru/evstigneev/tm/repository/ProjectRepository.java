package ru.evstigneev.tm.repository;

import ru.evstigneev.tm.entity.Project;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public class ProjectRepository {

    private Map<String, Project> projectMap = new LinkedHashMap<>();

    public Project persist(String projectName) {
        Project project = new Project(projectName, UUID.randomUUID().toString());
        projectMap.put(project.getId(), project);
        return project;
    }

    public boolean remove(String projectId) {
        return projectMap.remove(projectId) != null;
    }

    public Project update(String projectId, String projectName) {
        projectMap.get(projectId).setName(projectName);
        return projectMap.get(projectId);
    }

    public Map<String, Project> findAll() {
        return projectMap;
    }

    public Project findOne(String projectId) {
        return projectMap.get(projectId);
    }

    public Project merge(String projectId, String projectName) {
        if (findOne(projectId) != null) {
            return update(projectId, projectName);
        }
        return persist(projectName);
    }

    public void removeAll() {
        projectMap.clear();
    }

}
