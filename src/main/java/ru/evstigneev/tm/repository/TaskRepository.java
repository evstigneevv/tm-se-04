package ru.evstigneev.tm.repository;

import ru.evstigneev.tm.entity.Task;

import java.util.*;

public class TaskRepository {

    private Map<String, Task> taskMap = new LinkedHashMap<>();

    public Task persist(String taskName, String projectId) {
        Task task = new Task(taskName, UUID.randomUUID().toString(), projectId);
        return taskMap.put(task.getId(), task);
    }

    public Collection<Task> getTaskListByProjectId(String projectId) {
        List<Task> taskListByProjectId = new ArrayList<>();
        Collection<Task> taskList = taskMap.values();
        for (Task t : taskList) {
            if (t.getProjectId().equals(projectId)) {
                taskListByProjectId.add(t);
            }
        }
        return taskListByProjectId;
    }

    public Map<String, Task> findAll() {
        return taskMap;
    }

    public Task update(String taskId, String taskName) {
        taskMap.get(taskId).setName(taskName);
        return taskMap.get(taskId);
    }

    public boolean deleteAllProjectTasks(String projectId) {
        boolean isDeleted = false;
        Collection<Task> projectTasks = taskMap.values();
        for (Task t : projectTasks) {
            if (t.getProjectId().equals(projectId)) {
                taskMap.remove(t.getId());
                isDeleted = true;
            }
        }
        return isDeleted;
    }

    public boolean remove(String taskId) {
        return taskMap.remove(taskId) != null;
    }

    public Task findOne(String taskId) {
        return taskMap.get(taskId);
    }

    public Task merge(String taskId, String taskName) {
        if (findOne(taskId) != null) {
            return update(taskId, taskName);
        }
        return persist(taskName, findOne(taskId).getProjectId());
    }

    public void removeAll() {
        taskMap.clear();
    }

}