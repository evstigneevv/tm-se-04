package ru.evstigneev.tm.service;

import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.repository.ProjectRepository;
import ru.evstigneev.tm.repository.TaskRepository;

import javax.xml.bind.ValidationException;
import java.util.Map;

public class ProjectService {

    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;

    public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public Map<String, Project> findAll() throws ValidationException {
        if (projectRepository.findAll().isEmpty()) {
            throw new ValidationException("No projects!");
        }
        return projectRepository.findAll();
    }

    public Project persist(String projectName) throws ValidationException {
        if (projectName == null || projectName.isEmpty()) {
            throw new ValidationException("You entered an empty string!");
        }
        return projectRepository.persist(projectName);
    }

    public boolean remove(String projectId) throws ValidationException {
        if (projectId == null || projectId.isEmpty()) {
            throw new ValidationException("You entered an empty string!");
        }
        taskRepository.deleteAllProjectTasks(projectId);
        return projectRepository.remove(projectId);
    }

    public Project update(String projectId, String newProjectName) throws ValidationException {
        if (projectId == null || newProjectName == null || projectId.isEmpty() || newProjectName.isEmpty()) {
            throw new ValidationException("You entered an empty string!");
        }
        return projectRepository.update(projectId, newProjectName);
    }

    public Project findOne(String projectId) throws ValidationException {
        if (projectId == null || projectId.isEmpty()) {
            throw new ValidationException("You entered an empty string!");
        }
        return projectRepository.findOne(projectId);
    }

    public Project merge(String projectId, String projectName) throws ValidationException {
        if (projectId == null || projectName == null || projectId.isEmpty() || projectName.isEmpty()) {
            throw new ValidationException("You entered an empty string!");
        }
        return projectRepository.merge(projectId, projectName);
    }

    public void removeAll() {
        projectRepository.removeAll();
    }

}
