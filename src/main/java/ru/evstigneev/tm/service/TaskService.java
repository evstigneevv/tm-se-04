package ru.evstigneev.tm.service;

import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.repository.TaskRepository;

import javax.xml.bind.ValidationException;
import java.util.Collection;
import java.util.Map;

public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task persist(String taskName, String projectId) throws ValidationException {
        if (taskName == null || projectId == null || taskName.isEmpty() || projectId.isEmpty()) {
            throw new ValidationException("You entered an empty string!");
        }
        return taskRepository.persist(taskName, projectId);
    }

    public Map<String, Task> findAll() {
        return taskRepository.findAll();
    }

    public boolean remove(String taskId) throws ValidationException {
        if (taskId == null || taskId.isEmpty()) {
            throw new ValidationException("You entered an empty string!");
        }
        return taskRepository.remove(taskId);
    }

    public Collection<Task> getTaskListByProjectId(String projectId) throws ValidationException {
        if (projectId == null || projectId.isEmpty()) {
            throw new ValidationException("You entered an empty string!");
        }
        return taskRepository.getTaskListByProjectId(projectId);
    }

    public Task update(String taskId, String taskName) throws ValidationException {
        if (taskId == null || taskName == null || taskId.isEmpty() || taskName.isEmpty()) {
            throw new ValidationException("You entered an empty string!");
        }
        return taskRepository.update(taskId, taskName);
    }

    public Task findOne(String taskId) throws ValidationException {
        if (taskId == null || taskId.isEmpty()) {
            throw new ValidationException("You entered an empty string!");
        }
        return taskRepository.findOne(taskId);
    }

    public Task merge(String taskId, String taskName) throws ValidationException {
        if (taskId == null || taskName == null || taskId.isEmpty() || taskName.isEmpty()) {
            throw new ValidationException("You entered an empty string!");
        }
        return taskRepository.merge(taskId, taskName);
    }

    public void removeAll() {
        taskRepository.removeAll();
    }
}
